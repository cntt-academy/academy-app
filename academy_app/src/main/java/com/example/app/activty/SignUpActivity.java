package com.example.app.activty;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.example.app.R;
import com.example.app.databinding.LayoutBottomSheetMoreBinding;
import com.example.app.service.ApiService;
import com.example.app.service.request.SignUpRequest;
import com.example.app.service.response.JsonResponseBase;
import com.example.app.utils.AppUtils;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.apache.commons.lang3.StringUtils;

import java.util.Calendar;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {

    private final Calendar myCalendar = Calendar.getInstance();
    private final Context context = SignUpActivity.this;
    private final String TAG = "LoginActivity";
    private boolean isLeapYear = false;

    private EditText edtEmail, edtName, edtPhone;
    private AppCompatButton btnSignUp, btnBack;
    private TextView txtDateOfBirth;
    private RadioButton rbMale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initView();

        initListener();
    }

    private void initView() {
        txtDateOfBirth = findViewById(R.id.txtDateOfBirth);
        btnSignUp = findViewById(R.id.btnSignUp);
        edtPhone = findViewById(R.id.edtPhone);
        edtEmail = findViewById(R.id.edtEmail);
        edtName = findViewById(R.id.edtName);
        btnBack = findViewById(R.id.btnBack);
        rbMale = findViewById(R.id.rbMale);
    }

    private void initListener() {
        btnBack.setOnClickListener(view -> onBackPressed());

        btnSignUp.setOnClickListener(view -> {
            long time = System.currentTimeMillis();
            SignUpRequest request = initRequest(time);
            if (isRequestValid(request, time)) {
                doSignUp(request, time);
            }
        });

        txtDateOfBirth.setOnClickListener(view -> showBottomSheet());
    }

    private SignUpRequest initRequest(long time) {
        SignUpRequest request = new SignUpRequest();
        request.setAvatar("https://res.cloudinary.com/dvr6dfixz/image/upload/v1683108415/user/unknown_user.png");
        request.setName(edtName.getText().toString().trim());
        request.setEmail(edtEmail.getText().toString().trim());
        request.setPhone(edtPhone.getText().toString().trim());
        request.setDob(txtDateOfBirth.getText().toString().trim());
        if (rbMale.isChecked()) {
            request.setGender("Male");
        } else {
            request.setGender("Female");
        }

        Log.i(TAG, "Sign-up request " + time + ": " + request);
        return request;
    }

    private boolean isRequestValid(SignUpRequest request, long time) {
        if (StringUtils.isBlank(request.getName())
                || StringUtils.isBlank(request.getDob())
                || StringUtils.isBlank(request.getEmail())
                || StringUtils.isBlank(request.getPhone())) {
            Log.e(TAG, "Sign-up " + time + " error: Data is invalid");
            AppUtils.notification(context, getText(R.string.data_sign_up_invalid));
            return false;
        }

        String regName = "^[a-zA-Z ]+$";
        if (!request.getName().matches(regName)) {
            Log.e(TAG, "Sign-up " + time + " error: Name is invalid");
            AppUtils.notification(context, getText(R.string.name_invalid));
            return false;
        }

        String regEmail = "^[^\\s@]+@[^\\s@]+$";
        if (!request.getEmail().matches(regEmail)) {
            Log.e(TAG, "Sign-up " + time + " error: Email is invalid");
            AppUtils.notification(context, getText(R.string.email_invalid));
            return false;
        }

        String regPhone = "(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\\b";
        if (!request.getPhone().matches(regPhone)) {
            Log.e(TAG, "Sign-up " + time + " error: Phone is invalid");
            AppUtils.notification(context, getText(R.string.phone_invalid));
            return false;
        }
        return true;
    }

    private void doSignUp(SignUpRequest request, long time) {
        ApiService.apiService.signUp(request)
                .enqueue(new Callback<JsonResponseBase<Map<String, Object>>>() {
                    @Override
                    public void onResponse(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Response<JsonResponseBase<Map<String, Object>>> response) {
                        if (response.code() != 200 || response.body() == null) {
                            Log.e(TAG, "Sign-up " + time + " failed:" + response.message());
                            AppUtils.notification(context, getText(R.string.sign_up_failed));
                        } else {
                            Log.i(TAG, "Sign-up " + time + " success");
                            Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                            intent.putExtra("notification", getText(R.string.sign_up_success));
                            startActivity(intent);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Throwable t) {
                        Log.e(TAG, "Sign-up " + time + " error");
                        Log.getStackTraceString(t);
                        AppUtils.notification(context, getText(R.string.sign_up_failed));
                    }
                });
    }


    private void showBottomSheet() {
        LayoutBottomSheetMoreBinding bottomSheetBinding = LayoutBottomSheetMoreBinding.inflate(getLayoutInflater());
        BottomSheetDialog moreBottomSheet =
                new BottomSheetDialog(this);
        moreBottomSheet.setContentView(bottomSheetBinding.getRoot());

        bottomSheetBinding.namSinh.setMaxValue(2023);
        bottomSheetBinding.namSinh.setMinValue(1950);
        bottomSheetBinding.namSinh.setValue(myCalendar.get(Calendar.YEAR));
        bottomSheetBinding.namSinh.setWrapSelectorWheel(false);

        bottomSheetBinding.ngaySinh.setMaxValue(31);
        bottomSheetBinding.ngaySinh.setMinValue(1);
        bottomSheetBinding.ngaySinh.setValue(myCalendar.get(Calendar.DAY_OF_MONTH));
        bottomSheetBinding.ngaySinh.setWrapSelectorWheel(false);

        bottomSheetBinding.thangSinh.setMaxValue(12);
        bottomSheetBinding.thangSinh.setMinValue(1);
        bottomSheetBinding.thangSinh.setValue(myCalendar.get(Calendar.MONTH) + 1);
        bottomSheetBinding.thangSinh.setWrapSelectorWheel(false);

        isLeapYear = false;

        bottomSheetBinding.namSinh.setOnValueChangedListener((picker, oldVal, newVal) -> {
            int values = bottomSheetBinding.namSinh.getValue();
            if (values % 100 == 0) {
                isLeapYear = values % 400 == 0;
            } else {
                isLeapYear = values % 4 == 0;
            }
            if (isLeapYear) {
                if (bottomSheetBinding.thangSinh.getValue() == 2) {
                    bottomSheetBinding.ngaySinh.setMaxValue(29);
                }
            } else {
                if (bottomSheetBinding.thangSinh.getValue() == 2) {
                    bottomSheetBinding.ngaySinh.setMaxValue(28);
                }
            }
        });

        bottomSheetBinding.thangSinh.setOnValueChangedListener((picker, oldVal, newVal) -> {
            switch (bottomSheetBinding.thangSinh.getValue()) {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12: {
                    bottomSheetBinding.ngaySinh.setMaxValue(31);
                    break;
                }
                case 4:
                case 6:
                case 9:
                case 11: {
                    bottomSheetBinding.ngaySinh.setMaxValue(30);
                    break;
                }
                default: {
                    if (isLeapYear) {
                        bottomSheetBinding.ngaySinh.setMaxValue(29);
                    } else {
                        bottomSheetBinding.ngaySinh.setMaxValue(28);
                    }
                    break;
                }
            }
        });

        moreBottomSheet.setOnDismissListener(dialogInterface -> {
            String ngayThangNam = bottomSheetBinding.namSinh.getValue() + "-";
            if (bottomSheetBinding.thangSinh.getValue() < 10) {
                ngayThangNam += "0" + bottomSheetBinding.thangSinh.getValue() + "-";
            } else {
                ngayThangNam += bottomSheetBinding.thangSinh.getValue() + "-";
            }

            if (bottomSheetBinding.ngaySinh.getValue() < 10) {
                ngayThangNam += "0" + bottomSheetBinding.ngaySinh.getValue();
            } else {
                ngayThangNam += bottomSheetBinding.ngaySinh.getValue();
            }

            txtDateOfBirth.setText(ngayThangNam);

        });

        moreBottomSheet.show();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
        finish();
    }
}