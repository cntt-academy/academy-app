package com.example.app.activty;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import com.example.app.R;
import com.example.app.service.ApiService;
import com.example.app.service.response.JsonResponseBase;
import com.example.app.utils.AESUtils;
import com.example.app.utils.AppUtils;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPasswordActivity extends AppCompatActivity {

    private final Context context = ForgetPasswordActivity.this;
    private final String TAG = "ForgetPasswordActivity";

    private AppCompatButton btnConfirm, btnBack;
    private EditText edtEmail;

    @Override
    @RequiresApi(api = Build.VERSION_CODES.O)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        initView();

        initListener();
    }

    private void initView() {
        btnConfirm = findViewById(R.id.btnConfirm);
        edtEmail = findViewById(R.id.edtEmail);
        btnBack = findViewById(R.id.btnBack);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void initListener() {
        btnConfirm.setOnClickListener(view -> {
            long time = System.currentTimeMillis();
            String email = edtEmail.getText().toString().trim();
            Log.i(TAG, "Forget password request " + time + ": " + email);
            if (StringUtils.isBlank(email)) {
                Log.e(TAG, "Forget password " + time + " error: Email is blank");
                AppUtils.notification(context, getText(R.string.email_is_blank));
            } else {
                doForgetPassword(email, time);
            }
        });

        btnBack.setOnClickListener(view -> onBackPressed());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void doForgetPassword(String email, long time) {
        ApiService.apiService.forgetPassword(AESUtils.encrypt(email))
                .enqueue(new Callback<JsonResponseBase<Map<String, Object>>>() {
                    @Override
                    public void onResponse(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Response<JsonResponseBase<Map<String, Object>>> response) {
                        if (response.code() != 200 || response.body() == null) {
                            Log.e(TAG, "Forget password " + time + " failed:" + response.message());
                            AppUtils.notification(context, getText(R.string.forget_password_failed));
                        } else {
                            Log.i(TAG, "Forget password " + time + " success");
                            String message = Objects.requireNonNull(response.body().getMessage());

                            Intent intent = new Intent(ForgetPasswordActivity.this, LoginActivity.class);
                            intent.putExtra("notification", message);
                            startActivity(intent);

                            finish();
                        }
                    }

                    @Override
                    public void onFailure(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Throwable t) {
                        Log.e(TAG, "Forget password " + time + " error");
                        Log.getStackTraceString(t);
                        AppUtils.notification(context, getText(R.string.login_failed));
                    }
                });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(ForgetPasswordActivity.this, LoginActivity.class));
        finish();
    }
}