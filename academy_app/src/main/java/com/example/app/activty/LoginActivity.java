package com.example.app.activty;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.example.app.R;
//import com.example.app.activty.HomeActivity;
import com.example.app.service.ApiService;
import com.example.app.service.request.LoginRequest;
import com.example.app.service.response.JsonResponseBase;
import com.example.app.utils.AppConstants;
import com.example.app.utils.AppUtils;
import com.example.app.utils.SharedPreferenceUtil;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private final Context context = LoginActivity.this;
    private final String TAG = "LoginActivity";
    private boolean clickBack = false;

    private TextView txtForgetPassword, txtSignUp;
    private EditText edtUsername, edtPassword;
    private AppCompatButton btnLogin;
    private CheckBox cbRemember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initView();

        initListener();

        String notification = getIntent().getStringExtra("notification");
        if (StringUtils.isNotBlank(notification)) {
            AppUtils.notification(context, notification);
        }
    }

    private void initView() {
        txtForgetPassword = findViewById(R.id.txtForgetPassword);
        edtPassword = findViewById(R.id.edtPassword);
        edtUsername = findViewById(R.id.edtUsername);
        cbRemember = findViewById(R.id.cbRemember);
        txtSignUp = findViewById(R.id.txtSignUp);
        btnLogin = findViewById(R.id.btnLogin);
    }

    private void initListener() {
        btnLogin.setOnClickListener(view -> {
            long time = System.currentTimeMillis();
            LoginRequest request = initRequest(time);
            if (isRequestValid(request, time)) {
                doLogin(request, time);
            }
        });

        txtForgetPassword.setOnClickListener(view ->
                startActivity(new Intent(LoginActivity.this, ForgetPasswordActivity.class))
        );

        txtSignUp.setOnClickListener(view -> {
            startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
            finish();
        });
    }

    private LoginRequest initRequest(long time) {
        LoginRequest request = new LoginRequest();
        request.setUsername(edtUsername.getText().toString().trim());
        request.setPassword(edtPassword.getText().toString().trim());
        Log.i(TAG, "Login request " + time + ": " + request);
        return request;
    }

    private boolean isRequestValid(LoginRequest request, long time) {
        if (StringUtils.isBlank(request.getUsername())) {
            Log.e(TAG, "Sign-up " + time + " error: Username is invalid");
            AppUtils.notification(context, getText(R.string.username_is_blank));
            return false;
        }
        if (StringUtils.isBlank(request.getPassword())) {
            Log.e(TAG, "Sign-up " + time + " error: Password is invalid");
            AppUtils.notification(context, getText(R.string.password_is_blank));
            return false;
        }
        return true;
    }

    private void doLogin(LoginRequest request, long time) {
        ApiService.apiService.login(request)
                .enqueue(new Callback<JsonResponseBase<Map<String, Object>>>() {
                    @Override
                    public void onResponse(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Response<JsonResponseBase<Map<String, Object>>> response) {
                        if (response.code() != 200 || response.body() == null) {
                            Log.e(TAG, "Login " + time + " failed:" + response.message());
                            AppUtils.notification(context, getText(R.string.login_failed));
                        } else {
                            Log.i(TAG, "Login " + time + " success");
                            String token = (String) response.body().getData().get("token");
                            SharedPreferenceUtil.setString(AppConstants.TOKEN, token);

                            SharedPreferenceUtil.setString(AppConstants.USERNAME, request.getUsername());
                            SharedPreferenceUtil.setString(AppConstants.PASSWORD, request.getPassword());
                            SharedPreferenceUtil.setBoolean(AppConstants.REMEMBER, cbRemember.isChecked());

                            AppUtils.getLoggedAccountInfo(TAG);

                            redirectPage(request.getPassword());
                        }
                    }

                    @Override
                    public void onFailure(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Throwable t) {
                        Log.e(TAG, "Login " + time + " error");
                        Log.getStackTraceString(t);
                        AppUtils.notification(context, getText(R.string.login_failed));
                    }
                });
    }

    private void redirectPage(String password) {
        Intent intent;
        if (AppConstants.DEFAULT_PASSWORD.equalsIgnoreCase(password)) {
            intent = new Intent(LoginActivity.this, ChangePasswordActivity.class);
            intent.putExtra("resetPassword", true);
        } else {
            intent = new Intent(LoginActivity.this, HomeActivity.class);
        }
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (clickBack) {
            finish();
        } else {
            clickBack = true;
            AppUtils.notification(context, getText(R.string.click_back));
            new Handler().postDelayed(() -> clickBack = false, 1000L);
        }
    }
}