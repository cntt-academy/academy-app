package com.example.app.activty;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.app.R;
import com.example.app.adapter.ChatAdapter;
import com.example.app.model.Message;
import com.example.app.service.ApiService;
import com.example.app.service.response.JsonResponseBase;
import com.example.app.utils.AppConstants;
import com.example.app.utils.AppUtils;
import com.example.app.utils.Config;
import com.example.app.utils.GenericMapper;
import com.example.app.utils.SharedPreferenceUtil;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;

import org.modelmapper.ModelMapper;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends AppCompatActivity {

    private final GenericMapper genericMapper = new GenericMapper(new ModelMapper());
    private final List<Message> messages = new ArrayList<>();
    private final Context context = ChatActivity.this;
    private final String TAG = "ChatActivity";
    private final Gson gson = new Gson();

    private ChatAdapter chatAdapter;
    private EditText edtMessage;
    private ImageView btnSend;
    private ImageView btnBack;
    private ListView listView;
    private Integer roomId;
    private Socket socket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        verifyToken();

        if (!isOnline()) {
            AppUtils.notification(context, getText(R.string.requires_network));
        }

        try {
            socket = IO.socket(Config.URL_SOCKET);
            socket.connect();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        Intent intent = getIntent();
        roomId = intent.getIntExtra("roomId", 0);

        initView();

        initAdapter();

        initListener();

        getListMessage();
    }

    private Boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }

    private void initView() {
        TextView txtFriendName = findViewById(R.id.txtFriendName);
        edtMessage = findViewById(R.id.edtMessage);
        listView = findViewById(R.id.msgShow);
        btnBack = findViewById(R.id.btnBack);
        btnSend = findViewById(R.id.btnSend);

        Intent intent = getIntent();
        txtFriendName.setText(intent.getStringExtra("friendName"));
    }

    private void initAdapter() {
        chatAdapter = new ChatAdapter(ChatActivity.this, messages);
        listView.setAdapter(chatAdapter);
    }

    private void initListener() {
        socket.on("socket-typing", onSbIsTyping);
        socket.on("socket-stop-typing", onSbStopTyping);
        socket.on("server-send-message", onNewMessage);

        edtMessage.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                socket.emit("socket-typing");
            } else {
                socket.emit("socket-stop-typing");
            }
        });

        btnSend.setOnClickListener(v -> {
            if (edtMessage.getText().length() <= 0) {
                Toast.makeText(ChatActivity.this, "Input a message", Toast.LENGTH_LONG).show();
            } else {
                Message message = new Message();
                message.setContent(edtMessage.getText().toString());
                message.setRoomId(roomId);
                message.setAccountId(SharedPreferenceUtil.getInteger(AppConstants.ACCOUNT_ID));
                messages.add(message);

                edtMessage.setText("");
                listView.setSelection(messages.size() - 1);
                chatAdapter.notifyDataSetChanged();

                socket.emit("client-send-message", gson.toJson(message));
            }
        });

        btnBack.setOnClickListener(view -> finish());
    }

    private void verifyToken() {
        long time = System.currentTimeMillis();

        ApiService.apiService.verifyToken(AppUtils.generateHeaders())
                .enqueue(new Callback<JsonResponseBase<Map<String, Object>>>() {
                    @Override
                    public void onResponse(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Response<JsonResponseBase<Map<String, Object>>> response) {
                        if (response.code() != 200 || response.body() == null) {
                            Log.e(TAG, "Verify token " + time + " failed: " + response.message());
                            AppUtils.clearData();

                            Intent intent = new Intent(ChatActivity.this, LoginActivity.class);
                            intent.putExtra("notification", getText(R.string.token_time_out));
                            startActivity(intent);
                            finish();
                        } else {
                            Log.i(TAG, "Verify token " + time + " success");
                        }
                    }

                    @Override
                    public void onFailure(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Throwable t) {
                        Log.e(TAG, "Verify token " + time + " error");
                        Log.getStackTraceString(t);
                        AppUtils.clearData();

                        Intent intent = new Intent(ChatActivity.this, LoginActivity.class);
                        intent.putExtra("notification", getText(R.string.token_time_out));
                        startActivity(intent);
                        finish();
                    }
                });
    }



    private final Emitter.Listener onSbIsTyping = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(() -> edtMessage.setHint("Someone is typing..."));
        }
    };

    private final Emitter.Listener onSbStopTyping = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(() -> edtMessage.setHint("Write a message..."));
        }
    };

    private final Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(() -> {
                Message message = gson.fromJson(args[0].toString(), Message.class);
                if (!SharedPreferenceUtil
                        .getInteger(AppConstants.ACCOUNT_ID)
                        .equals(message.getAccountId())) {
                    messages.add(message);
                    chatAdapter.notifyDataSetChanged();
                }
                listView.setSelection(chatAdapter.getCount() - 1);
            });
        }
    };

    private void getListMessage() {
        long time = System.currentTimeMillis();
        Log.e(TAG, "Get list message " + time + " roomId: " + roomId);

        ApiService.apiService.getListMessage(AppUtils.generateHeaders(), roomId)
                .enqueue(new Callback<JsonResponseBase<Map<String, Object>>>() {
                    @Override
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    public void onResponse(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Response<JsonResponseBase<Map<String, Object>>> response) {
                        if (response.code() != 200 || response.body() == null) {
                            Log.e(TAG, "Get list message " + time + " failed:" + response.message());
                        } else {
                            Log.i(TAG, "Get list message " + time + " success");
                            List<Message> tmpMessages = genericMapper.mapToListOfType(
                                        (List<?>) response.body().getData().get("messages"),
                                        Message.class);
                            if (tmpMessages != null && tmpMessages.size() > 0) {
                                messages.addAll(tmpMessages);
                                chatAdapter.notifyDataSetChanged();
                                listView.setSelection(messages.size() - 1);
                            }
                        }
                    }

                    @Override
                    public void onFailure(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Throwable t) {
                        Log.e(TAG, "Get list message " + time + " error");
                        Log.getStackTraceString(t);
                    }
                });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socket.disconnect();
    }
}
