package com.example.app.activty;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app.R;
import com.example.app.action.AccountAction;
import com.example.app.action.RoomChatAction;
import com.example.app.adapter.AccountAdapter;
import com.example.app.adapter.RoomChatAdapter;
import com.example.app.model.Account;
import com.example.app.model.Message;
import com.example.app.model.RoomChat;
import com.example.app.service.ApiService;
import com.example.app.service.response.JsonResponseBase;
import com.example.app.utils.AppConstants;
import com.example.app.utils.AppUtils;
import com.example.app.utils.Config;
import com.example.app.utils.GenericMapper;
import com.example.app.utils.SharedPreferenceUtil;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements AccountAction, RoomChatAction {

    private final GenericMapper genericMapper = new GenericMapper(new ModelMapper());
    private final List<RoomChat> roomChats = new ArrayList<>();
    private final Context context = HomeActivity.this;
    private final String TAG = "HomeActivity";
    private final Gson gson = new Gson();
    private boolean clickBack = false;

    private RecyclerView rcyAccountSearch, rcyRoomChat;
    private EditText edtSearch;
    private ImageView imgMenu;
    private View viewNull;

    private RoomChatAdapter roomChatAdapter;
    private AccountAdapter accountAdapter;
    private Socket socket;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        verifyToken();

        if (!isOnline()) {
            AppUtils.notification(context, getText(R.string.requires_network));
        }

        try {
            socket = IO.socket(Config.URL_SOCKET);
            socket.connect();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        initView();

        initAdapter();

        initListener();
    }

    private Boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }

    private void initView() {
        imgMenu = findViewById(R.id.imgMenu);
        viewNull = findViewById(R.id.viewNull);
        edtSearch = findViewById(R.id.edtSearch);
        rcyRoomChat = findViewById(R.id.rcyRoomChat);
        rcyAccountSearch = findViewById(R.id.rcyAccountSearch);
    }

    private void initAdapter() {
        roomChatAdapter = new RoomChatAdapter(HomeActivity.this, roomChats, this);
        rcyRoomChat.setAdapter(roomChatAdapter);
        getListRoom();

        accountAdapter = new AccountAdapter(HomeActivity.this, new ArrayList<>(), this);
        rcyAccountSearch.setAdapter(accountAdapter);
    }

    private void initListener() {
        socket.on("server-send-message", onNewMessage);

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (StringUtils.isNotBlank(charSequence.toString())) {
                    findAccountByKey(charSequence.toString());
                    rcyAccountSearch.setVisibility(View.VISIBLE);
                    rcyRoomChat.setVisibility(View.GONE);
                } else {
                    rcyAccountSearch.setVisibility(View.GONE);
                    rcyRoomChat.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        viewNull.setOnClickListener(view -> accountAdapter.setData(new ArrayList<>()));

        imgMenu.setOnClickListener(this::popupMenu);

    }

    private final Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        @SuppressLint("NotifyDataSetChanged")
        @RequiresApi(api = Build.VERSION_CODES.N)
        public void call(final Object... args) {
            runOnUiThread(() -> {
                Message message = gson.fromJson(args[0].toString(), Message.class);
                RoomChat chatToUpdate = null;
                for (RoomChat index : roomChats) {
                    if (index.getId().equals(message.getRoomId())) {
                        index.setLastMessage(message.getContent());
                        index.setSendTime(message.getSendTime());
                        chatToUpdate = index;
                        break;
                    }
                }
                if (chatToUpdate != null) {
                    roomChats.remove(chatToUpdate);
                    roomChats.add(0, chatToUpdate);
                }
                roomChatAdapter.notifyDataSetChanged();
            });
        }
    };

    private void verifyToken() {
        long time = System.currentTimeMillis();

        ApiService.apiService.verifyToken(AppUtils.generateHeaders())
                .enqueue(new Callback<JsonResponseBase<Map<String, Object>>>() {
                    @Override
                    public void onResponse(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Response<JsonResponseBase<Map<String, Object>>> response) {
                        if (response.code() != 200 || response.body() == null) {
                            Log.e(TAG, "Verify token " + time + " failed: " + response.message());
                            AppUtils.clearData();

                            Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                            intent.putExtra("notification", getText(R.string.token_time_out));
                            startActivity(intent);
                            finish();
                        } else {
                            Log.i(TAG, "Verify token " + time + " success");
                        }
                    }

                    @Override
                    public void onFailure(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Throwable t) {
                        Log.e(TAG, "Verify token " + time + " error");
                        Log.getStackTraceString(t);
                        AppUtils.clearData();

                        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                        intent.putExtra("notification", getText(R.string.token_time_out));
                        startActivity(intent);
                        finish();
                    }
                });
    }

    private void getListRoom() {
        long time = System.currentTimeMillis();
        Log.e(TAG, "Get list room " + time + " by accountId: " + SharedPreferenceUtil.getInteger(AppConstants.ACCOUNT_ID));

        ApiService.apiService.getListRoom(AppUtils.generateHeaders())
                .enqueue(new Callback<JsonResponseBase<Map<String, Object>>>() {
                    @Override
                    @SuppressLint("NotifyDataSetChanged")
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    public void onResponse(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Response<JsonResponseBase<Map<String, Object>>> response) {
                        if (response.code() != 200 || response.body() == null) {
                            Log.e(TAG, "Get list room " + time + " failed:" + response.message());
                        } else {
                            Log.i(TAG, "Get list room " + time + " success");
                            List<RoomChat> tmpRoomChats =
                                    genericMapper.mapToListOfType(
                                            (List<?>) response.body().getData().get("rooms"),
                                            RoomChat.class);
                            if (tmpRoomChats != null && tmpRoomChats.size() > 0) {
                                roomChats.addAll(tmpRoomChats);
                                roomChatAdapter.notifyDataSetChanged();
                            }
                        }
                    }

                    @Override
                    public void onFailure(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Throwable t) {
                        Log.e(TAG, "Get list room " + time + " error");
                        Log.getStackTraceString(t);
                    }
                });
    }

    private void findAccountByKey(String key) {
        long time = System.currentTimeMillis();
        Log.e(TAG, "Find account by key " + time + ":" + key);

        ApiService.apiService.findAccountByKey(AppUtils.generateHeaders(), key)
                .enqueue(new Callback<JsonResponseBase<Map<String, Object>>>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onResponse(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Response<JsonResponseBase<Map<String, Object>>> response) {
                        if (response.code() != 200 || response.body() == null) {
                            Log.e(TAG, "Find account by key " + time + " failed:" + response.message());
                        } else {
                            Log.i(TAG, "Find account by key " + time + " success");
                            Map<String, Object> data = response.body().getData();
                            List<Account> accounts =
                                    genericMapper.mapToListOfType(
                                            (List<?>) data.get("accounts"),
                                            Account.class);
                            if (accounts != null && accounts.size() > 0) {
                                accountAdapter.setData(accounts);
                            }
                        }
                    }

                    @Override
                    public void onFailure(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Throwable t) {
                        Log.e(TAG, "Find account by key " + time + " error");
                        Log.getStackTraceString(t);
                    }
                });
    }

    @SuppressLint("NonConstantResourceId")
    private void popupMenu(View view) {
        PopupMenu popupMenu = new PopupMenu(this, view);
        popupMenu.getMenuInflater().inflate(R.menu.popup_menu, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.accountInfo:
                    startActivity(new Intent(HomeActivity.this, InformationActivity.class));
                    break;
                case R.id.changePassword:
                    startActivity(new Intent(HomeActivity.this, ChangePasswordActivity.class));
                    break;
                case R.id.logout:
                    AppUtils.logout(TAG);
                    startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                    finish();
                    break;
            }
            return true;
        });
        popupMenu.show();
    }

    @Override
    public void clickAccount(Account account) {
        findRoomWithFriend(account.getId());
    }

    private void findRoomWithFriend(int friendId) {
        long time = System.currentTimeMillis();
        Log.e(TAG, "Find room with friend " + time + " request: " + friendId);

        ApiService.apiService.findRoomWithFriend(AppUtils.generateHeaders(), friendId)
                .enqueue(new Callback<JsonResponseBase<Map<String, Object>>>() {
                    @Override
                    public void onResponse(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Response<JsonResponseBase<Map<String, Object>>> response) {
                        if (response.code() != 200 || response.body() == null) {
                            Log.e(TAG, "Find room with friend " + time + " failed:" + response.message());
                        } else {
                            Log.i(TAG, "Find room with friend " + time + " success");
                            Map<?, ?> room = (Map<?, ?>) response.body().getData().get("room");
                            Double roomId = (Double) Objects.requireNonNull(room).get("id");

                            socket.emit("client-join-room", Objects.requireNonNull(roomId).intValue());
                            Intent intent = new Intent(context, ChatActivity.class);
                            intent.putExtra("roomId", roomId.intValue());
                            intent.putExtra("friendName", String.valueOf(room.get("friendName")));
                            accountAdapter.setData(new ArrayList<>());
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Throwable t) {
                        Log.e(TAG, "Find room with friend " + time + " error");
                        Log.getStackTraceString(t);
                    }
                });
    }

    @Override
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void clickRoomChat(RoomChat roomChat) {
        socket.emit("client-join-room", roomChat.getId());
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("roomId", roomChat.getId());
        intent.putExtra("friendName", roomChat.getFriendName());

        CompletableFuture.runAsync(() -> markAsRead(roomChat.getId()));

        startActivity(intent);
    }

    private void markAsRead(Integer roomId) {
        long time = System.currentTimeMillis();
        Log.e(TAG, "Mark as read message " + time + " roomId: " + roomId);

        ApiService.apiService.markAsRead(AppUtils.generateHeaders(), roomId)
                .enqueue(new Callback<JsonResponseBase<Map<String, Object>>>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onResponse(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Response<JsonResponseBase<Map<String, Object>>> response) {
                        if (response.code() != 200 || response.body() == null) {
                            Log.e(TAG, "Mark as read message " + time + " failed:" + response.message());
                        } else {
                            Log.i(TAG, "Mark as read message " + time + " success");
                        }
                    }

                    @Override
                    public void onFailure(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Throwable t) {
                        Log.e(TAG, "Mark as read message " + time + " error");
                        Log.getStackTraceString(t);
                    }
                });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }


    @Override
    public void onBackPressed() {
        if (clickBack) {
            finish();
        } else {
            clickBack = true;
            AppUtils.notification(context, getText(R.string.click_back));
            new Handler().postDelayed(() -> clickBack = false, 1000L);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        edtSearch.setText("");
        rcyAccountSearch.setVisibility(View.GONE);
        rcyRoomChat.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socket.disconnect();
    }
}