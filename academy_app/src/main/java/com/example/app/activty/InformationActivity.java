package com.example.app.activty;

import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.app.R;
import com.example.app.utils.AppConstants;
import com.example.app.utils.AppUtils;
import com.example.app.utils.SharedPreferenceUtil;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;

public class InformationActivity extends AppCompatActivity {

    private final String TAG = "InformationActivity";

    private TextView txtName, txtGender, txtDob, txtPhone, txtEmail, txtCreatedDate;
    private ImageView imgAvatar, imgBack;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);

        CompletableFuture.runAsync(() -> AppUtils.getLoggedAccountInfo(TAG));

        initView();

        initListener();
    }

    private void initView() {
        txtName = findViewById(R.id.txtName);
        txtGender = findViewById(R.id.txtGender);
        txtDob = findViewById(R.id.txtDob);
        txtPhone = findViewById(R.id.txtPhone);
        txtEmail = findViewById(R.id.txtEmail);
        txtCreatedDate = findViewById(R.id.txtCreatedDate);

        imgAvatar = findViewById(R.id.imgAvatar);
        imgBack = findViewById(R.id.imgBack);
    }

    private void initListener() {
        imgBack.setOnClickListener(view -> onBackPressed());
    }

    @Override
    protected void onResume() {
        super.onResume();

        Glide.with(this)
                .load(SharedPreferenceUtil.getString(AppConstants.AVATAR))
                .into(imgAvatar);

        txtName.setText(SharedPreferenceUtil.getString(AppConstants.NAME));
        if (Objects.equals(SharedPreferenceUtil.getString(AppConstants.GENDER), "Female")) {
            txtGender.setText(getText(R.string.female));
        } else {
            txtGender.setText(getText(R.string.male));
        }
        txtDob.setText(SharedPreferenceUtil.getString(AppConstants.DOB));
        txtEmail.setText(SharedPreferenceUtil.getString(AppConstants.EMAIL));
        txtPhone.setText(SharedPreferenceUtil.getString(AppConstants.PHONE));
        txtCreatedDate.setText(SharedPreferenceUtil.getString(AppConstants.CREATED_DATE));
    }
}