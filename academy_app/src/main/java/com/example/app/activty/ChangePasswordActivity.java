package com.example.app.activty;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.example.app.R;
import com.example.app.service.ApiService;
import com.example.app.service.request.ChangePasswordRequest;
import com.example.app.service.response.JsonResponseBase;
import com.example.app.utils.AppConstants;
import com.example.app.utils.AppUtils;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {

    private final Context context = ChangePasswordActivity.this;
    private final String TAG = "ChangePassActivity";
    private Boolean resetPassword = false;

    private EditText edtOldPassword, edtNewPassword, edtConfirmPassword;
    private AppCompatButton btnConfirm, btnBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        resetPassword = getIntent().getBooleanExtra("resetPassword", false);

        initView();

        initListener();
    }

    private void initView() {
        RelativeLayout rlOldPassWord = findViewById(R.id.rlOldPassword);
        edtConfirmPassword = findViewById(R.id.edtConfirmPassword);
        edtNewPassword = findViewById(R.id.edtNewPassword);
        edtOldPassword = findViewById(R.id.edtOldPassword);
        btnConfirm = findViewById(R.id.btnConfirm);
        btnBack = findViewById(R.id.btnBack);

        if (resetPassword) {
            btnBack.setVisibility(View.GONE);
            rlOldPassWord.setVisibility(View.GONE);
        }
    }

    private void initListener() {
        btnBack.setOnClickListener(view -> onBackPressed());

        btnConfirm.setOnClickListener(view -> {
            long time = System.currentTimeMillis();
            ChangePasswordRequest request = initRequest(time);
            if (isRequestValid(request, time)) {
                doChangePassword(request, time);
            }
        });
    }

    private ChangePasswordRequest initRequest(long time) {
        ChangePasswordRequest request = new ChangePasswordRequest();
        if (resetPassword) {
            request.setOldPassword(AppConstants.DEFAULT_PASSWORD);
        } else {
            request.setOldPassword(edtOldPassword.getText().toString().trim());
        }
        request.setNewPassword(edtNewPassword.getText().toString().trim());
        request.setConfirmPassword(edtConfirmPassword.getText().toString().trim());
        Log.i(TAG, "Change password request " + time + ": " + request);
        return request;
    }

    private boolean isRequestValid(ChangePasswordRequest request, long time) {
        if (!resetPassword && StringUtils.isBlank(request.getOldPassword())) {
            Log.e(TAG, "Change password " + time + " error: Old password is blank");
            AppUtils.notification(context, getText(R.string.old_password_is_blank));
            return false;
        }

        if (!resetPassword && request.getNewPassword().equals(request.getOldPassword())) {
            Log.e(TAG, "Change password " + time + " error: Password is not changed");
            AppUtils.notification(context, getText(R.string.password_not_changed));
            return false;
        }

        if (StringUtils.isBlank(request.getNewPassword())) {
            Log.e(TAG, "Change password " + time + " error: New password is blank");
            AppUtils.notification(context, getText(R.string.new_password_is_blank));
            return false;
        }

        if (StringUtils.isBlank(request.getConfirmPassword())) {
            Log.e(TAG, "Change password " + time + " error: Confirm password is blank");
            AppUtils.notification(context, getText(R.string.confirm_password_is_blank));
            return false;
        }

        if (!request.getNewPassword().equals(request.getConfirmPassword())) {
            Log.e(TAG, "Change password " + time + " error: Confirm password error");
            AppUtils.notification(context, getText(R.string.confirm_password_error));
            return false;
        }

        return true;
    }

    private void doChangePassword(ChangePasswordRequest request, long time) {
        ApiService.apiService.changePassword(AppUtils.generateHeaders(), request)
                .enqueue(new Callback<JsonResponseBase<Map<String, Object>>>() {
                    @Override
                    public void onResponse(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Response<JsonResponseBase<Map<String, Object>>> response) {
                        call.request();
                        if (response.code() != 200 || response.body() == null) {
                            Log.e(TAG, "Change password " + time + " failed:" + response.message());
                            AppUtils.notification(context, getText(R.string.change_password_failed));
                        } else {
                            Log.i(TAG, "Change password " + time + " success");
                            AppUtils.notification(context, getText(R.string.change_password_success));
                            startActivity(new Intent(ChangePasswordActivity.this, LoginActivity.class));
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Throwable t) {
                        Log.e(TAG, "Change password " + time + " error");
                        Log.getStackTraceString(t);
                        AppUtils.notification(context, getText(R.string.login_failed));
                    }
                });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}