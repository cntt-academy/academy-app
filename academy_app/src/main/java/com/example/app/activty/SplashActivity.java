package com.example.app.activty;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.app.R;
//import com.example.app.activty.HomeActivity;
import com.example.app.service.ApiService;
import com.example.app.service.request.LoginRequest;
import com.example.app.service.response.JsonResponseBase;
import com.example.app.utils.AppConstants;
import com.example.app.utils.AppUtils;
import com.example.app.utils.SharedPreferenceUtil;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("CustomSplashScreen")
public class SplashActivity extends AppCompatActivity {

    private final String TAG = "SplashActivity";
    private final long delay = 1500L;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (SharedPreferenceUtil.getBoolean(AppConstants.REMEMBER)) {
            doLogin();
        } else {
            startLogIn();
        }
    }

    private void doLogin() {
        long time = System.currentTimeMillis();

        LoginRequest request = new LoginRequest();
        request.setUsername(SharedPreferenceUtil.getString(AppConstants.USERNAME));
        request.setPassword(SharedPreferenceUtil.getString(AppConstants.PASSWORD));
        Log.i(TAG, "Login request " + time + ": " + request);

        ApiService.apiService.login(request)
                .enqueue(new Callback<JsonResponseBase<Map<String, Object>>>() {
                    @Override
                    public void onResponse(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Response<JsonResponseBase<Map<String, Object>>> response) {
                        Log.i(TAG, "Request: " + call.request().body());
                        if (response.code() != 200 || response.body() == null) {
                            Log.e(TAG, "Login " + time + " failed:" + response.message());
                            startLogIn();
                        } else {
                            Log.i(TAG, "Login " + time + " success");
                            String token = (String) response.body().getData().get("token");
                            SharedPreferenceUtil.setString(AppConstants.TOKEN, token);
                            AppUtils.getLoggedAccountInfo(TAG);
                            startHome();
                        }
                    }

                    @Override
                    public void onFailure(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Throwable t) {
                        Log.e(TAG, "Login " + time + " error");
                        Log.getStackTraceString(t);
                        startLogIn();
                    }
                });
    }

    private void startHome() {
        new Handler().postDelayed(() -> {
            startActivity(new Intent(SplashActivity.this, HomeActivity.class));
            finish();
        }, delay);
    }

    private void startLogIn() {
        new Handler().postDelayed(() -> {
            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
            finish();
        }, delay);
    }
}