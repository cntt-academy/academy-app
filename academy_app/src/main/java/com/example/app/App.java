package com.example.app;

import android.app.Application;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.app.utils.SharedPreferenceUtil;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class App extends Application {
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferenceUtil.init(this);
    }
}
