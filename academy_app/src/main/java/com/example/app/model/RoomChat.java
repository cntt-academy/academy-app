package com.example.app.model;

import androidx.annotation.NonNull;

public class RoomChat {

    private Integer id;
    private String name;
    private String type;
    private String friendAvatar;
    private String friendName;
    private String friendEmail;
    private String lastMessage;
    private String sendTime;
    private Boolean isReadLastMessage;
    private String createdDate;

    public RoomChat() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFriendAvatar() {
        return friendAvatar;
    }

    public void setFriendAvatar(String friendAvatar) {
        this.friendAvatar = friendAvatar;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public String getFriendEmail() {
        return friendEmail;
    }

    public void setFriendEmail(String friendEmail) {
        this.friendEmail = friendEmail;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public Boolean getReadLastMessage() {
        return isReadLastMessage;
    }

    public void setReadLastMessage(Boolean readLastMessage) {
        isReadLastMessage = readLastMessage;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    @NonNull
    @Override
    public String toString() {
        return "RoomChat{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", friendAvatar='" + friendAvatar + '\'' +
                ", friendName='" + friendName + '\'' +
                ", friendEmail='" + friendEmail + '\'' +
                ", lastMessage='" + lastMessage + '\'' +
                ", sendTime='" + sendTime + '\'' +
                ", isReadLastMessage=" + isReadLastMessage +
                ", createdDate='" + createdDate + '\'' +
                '}';
    }
}
