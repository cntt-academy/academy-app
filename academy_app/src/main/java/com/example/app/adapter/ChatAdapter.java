package com.example.app.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.app.R;
import com.example.app.model.Message;
import com.example.app.utils.AppConstants;
import com.example.app.utils.SharedPreferenceUtil;

import java.util.List;

public class ChatAdapter extends BaseAdapter {

    List<Message> messages;
    Context context;

    public ChatAdapter(Context context, List<Message> messages) {
        this.context = context;
        this.messages = messages;
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    @SuppressLint("InflateParams")
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Message message = messages.get(position);

        if (SharedPreferenceUtil.getInteger(AppConstants.ACCOUNT_ID).equals(message.getAccountId())) {
            convertView = inflater.inflate(R.layout.message_send, null);
            TextView txtSend = convertView.findViewById(R.id.txtMsgSend);
            txtSend.setText(messages.get(position).getContent());
        } else {
            convertView = inflater.inflate(R.layout.message_received, null);
            TextView txtMsgReceived = convertView.findViewById(R.id.txtMsgReceived);
            TextView txtNameReceived = convertView.findViewById(R.id.txtFriendName);
            txtNameReceived.setText(messages.get(position).getAccountName());
            txtMsgReceived.setText(messages.get(position).getContent());
        }
        return convertView;
    }
}
