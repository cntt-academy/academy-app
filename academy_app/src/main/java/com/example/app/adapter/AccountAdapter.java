package com.example.app.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.app.R;
import com.example.app.action.AccountAction;
import com.example.app.model.Account;

import java.util.List;

public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.ViewHolder> {

    private final AccountAction accountAction;
    private final List<Account> accounts;
    private final Context context;


    public AccountAdapter(Context context, List<Account> accounts, AccountAction accountAction) {
        this.accountAction = accountAction;
        this.accounts = accounts;
        this.context = context;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<Account> data) {
        this.accounts.clear();
        this.accounts.addAll(data);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item_account, parent, false);
        return new ViewHolder(view);
    }


    @Override
    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        Account account = accounts.get(position);
        holder.txtAccountName.setText(account.getName());
        holder.txtAccountEmail.setText(account.getEmail());

        Glide.with(holder.itemView)
                .load(account.getAvatar())
                .into(holder.imgAvatar);

        holder.viewAccount.setOnClickListener(view -> accountAction.clickAccount(account));
    }

    @Override
    public int getItemCount() {
        return accounts.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView txtAccountName, txtAccountEmail;
        private final LinearLayout viewAccount;
        private final ImageView imgAvatar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtAccountEmail = itemView.findViewById(R.id.txtAccountEmail);
            txtAccountName = itemView.findViewById(R.id.txtAccountName);
            viewAccount = itemView.findViewById(R.id.viewAccount);
            imgAvatar = itemView.findViewById(R.id.imgAvatar);
        }
    }
}
