package com.example.app.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.app.R;
import com.example.app.action.RoomChatAction;
import com.example.app.model.RoomChat;

import java.util.List;

public class RoomChatAdapter extends RecyclerView.Adapter<RoomChatAdapter.ViewHolder> {

    private final RoomChatAction roomChatAction;
    private final List<RoomChat> roomChats;
    private final Context context;

    public RoomChatAdapter(Context context, List<RoomChat> roomChats, RoomChatAction roomChatAction) {
        this.roomChatAction = roomChatAction;
        this.roomChats = roomChats;
        this.context = context;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<RoomChat> data) {
        this.roomChats.clear();
        this.roomChats.addAll(data);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RoomChatAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item_room_chat, parent, false);
        return new ViewHolder(view);
    }


    @Override
    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint({"SetTextI18n", "ResourceAsColor"})
    public void onBindViewHolder(@NonNull RoomChatAdapter.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        RoomChat roomChat = roomChats.get(position);
        holder.txtFriendName.setText(roomChat.getFriendName());
        holder.txtLastMessage.setText(roomChat.getLastMessage() + " - " + roomChat.getSendTime());

        Glide.with(holder.itemView)
                .load(roomChat.getFriendAvatar())
                .into(holder.imgFriendAvatar);

        if (!roomChat.getReadLastMessage()) {
            holder.txtLastMessage.setTypeface(null, Typeface.BOLD_ITALIC);
        }

        holder.viewRoomChat.setOnClickListener(view -> {
            holder.txtLastMessage.setTypeface(null, Typeface.NORMAL);
            roomChatAction.clickRoomChat(roomChat);
        });
    }


    @Override
    public int getItemCount() {
        return roomChats.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView txtLastMessage, txtFriendName;
        private final LinearLayout viewRoomChat;
        private final ImageView imgFriendAvatar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgFriendAvatar = itemView.findViewById(R.id.imgFriendAvatar);
            txtLastMessage = itemView.findViewById(R.id.txtLastMessage);
            txtFriendName = itemView.findViewById(R.id.txtFriendName);
            viewRoomChat = itemView.findViewById(R.id.viewRoomChat);
        }
    }
}
