package com.example.app.action;

import com.example.app.model.Account;

public interface AccountAction {
    void clickAccount(Account account);
}
