package com.example.app.action;

import com.example.app.model.RoomChat;

public interface RoomChatAction {
    void clickRoomChat(RoomChat roomChat);
}
