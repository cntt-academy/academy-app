package com.example.app.service.request;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.List;

public class CreateRoomRequest implements Serializable {

    private List<Integer> accountIds;

    public CreateRoomRequest() {
    }

    public List<Integer> getAccountIds() {
        return accountIds;
    }

    public void setAccountIds(List<Integer> accountIds) {
        this.accountIds = accountIds;
    }

    @NonNull
    @Override
    public String toString() {
        return "CreateRoomRequest{" +
                "accountIds=" + accountIds +
                '}';
    }
}