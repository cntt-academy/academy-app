package com.example.app.service;

import com.example.app.service.request.ChangePasswordRequest;
import com.example.app.service.request.LoginRequest;
import com.example.app.service.request.SignUpRequest;
import com.example.app.service.response.JsonResponseBase;
import com.example.app.utils.Config;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface ApiService {

    ApiService apiService = new Retrofit.Builder()
            .baseUrl(Config.URL_API)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService.class);

    @POST("/authentication/login")
    Call<JsonResponseBase<Map<String, Object>>> login(@Body LoginRequest request);

    @GET("/authentication/logout")
    Call<JsonResponseBase<Map<String, Object>>> logout(@HeaderMap Map<String, String> headers);

    @GET("/authentication/verifyToken")
    Call<JsonResponseBase<Map<String, Object>>> verifyToken(@HeaderMap Map<String, String> headers);

    @GET("/account/forgetPassword")
    Call<JsonResponseBase<Map<String, Object>>> forgetPassword(@Query("email") String email);

    @PUT("/account/changePassword")
    Call<JsonResponseBase<Map<String, Object>>> changePassword(
            @HeaderMap Map<String, String> headers,
            @Body ChangePasswordRequest request);

    @GET("/account/getLoggedInfo")
    Call<JsonResponseBase<Map<String, Object>>> getLoggedAccountInfo(
            @HeaderMap Map<String, String> headers);

    @GET("/account/findByKey")
    Call<JsonResponseBase<Map<String, Object>>> findAccountByKey(
            @HeaderMap Map<String, String> headers,
            @Query("key") String key);

    @POST("/user/create")
    Call<JsonResponseBase<Map<String, Object>>> signUp(@Body SignUpRequest request);

    @GET("/room/getList")
    Call<JsonResponseBase<Map<String, Object>>> getListRoom(@HeaderMap Map<String, String> headers);

    @GET("/room/findRoomWithFriend")
    Call<JsonResponseBase<Map<String, Object>>> findRoomWithFriend(
            @HeaderMap Map<String, String> headers,
            @Query("friendId") Integer friendId);

    @GET("/message/getList")
    Call<JsonResponseBase<Map<String, Object>>> getListMessage(
            @HeaderMap Map<String, String> headers,
            @Query("roomId") Integer roomId);

    @GET("/message/markAsRead")
    Call<JsonResponseBase<Map<String, Object>>> markAsRead(
            @HeaderMap Map<String, String> headers,
            @Query("roomId") Integer roomId);
}
