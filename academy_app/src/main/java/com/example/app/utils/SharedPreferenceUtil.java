package com.example.app.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceUtil {
    public static SharedPreferences sharedPreferences;

    public static void init(Context context) {
        sharedPreferences = context.getSharedPreferences("data_app", Context.MODE_PRIVATE);
    }

    @SuppressLint("CommitPrefEdits")
    public static void setBoolean(String key, Boolean data) {
        sharedPreferences.edit().putBoolean(key, data).apply();
    }

    public static Boolean getBoolean(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    @SuppressLint("CommitPrefEdits")
    public static void setString(String key, String data) {
        sharedPreferences.edit().putString(key, data).apply();
    }

    public static String getString(String key) {
        return sharedPreferences.getString(key, "");
    }

    @SuppressLint("CommitPrefEdits")
    public static void setInteger(String key, Integer data) {
        sharedPreferences.edit().putInt(key, data).apply();
    }

    public static Integer getInteger(String key) {
        return sharedPreferences.getInt(key, 0);
    }
}
