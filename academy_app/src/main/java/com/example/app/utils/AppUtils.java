package com.example.app.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.app.service.ApiService;
import com.example.app.service.response.JsonResponseBase;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppUtils {
    public static void notification(Context context, CharSequence message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static Map<String, String> generateHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Bearer " + SharedPreferenceUtil.getString(AppConstants.TOKEN));
        return headers;
    }

    public static void clearData() {
        SharedPreferenceUtil.setString(AppConstants.TOKEN, null);
        SharedPreferenceUtil.setInteger(AppConstants.ACCOUNT_ID, 0);
        SharedPreferenceUtil.setString(AppConstants.AVATAR, null);
        SharedPreferenceUtil.setString(AppConstants.NAME, null);
        SharedPreferenceUtil.setString(AppConstants.GENDER, null);
        SharedPreferenceUtil.setString(AppConstants.DOB, null);
        SharedPreferenceUtil.setString(AppConstants.PHONE, null);
        SharedPreferenceUtil.setString(AppConstants.ROLE, null);

    }

    public static void logout(String tag) {
        long time = System.currentTimeMillis();

        ApiService.apiService.logout(AppUtils.generateHeaders())
                .enqueue(new Callback<JsonResponseBase<Map<String, Object>>>() {
                    @Override
                    public void onResponse(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Response<JsonResponseBase<Map<String, Object>>> response) {
                        if (response.code() != 200 || response.body() == null) {
                            Log.e(tag, "Verify token " + time + " failed:" + response.message());
                        } else {
                            Log.i(tag, "Verify token " + time + " success");
                        }
                    }

                    @Override
                    public void onFailure(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Throwable t) {
                        Log.e(tag, "Verify token " + time + " error");
                        Log.getStackTraceString(t);
                    }
                });
    }

    public static void getLoggedAccountInfo(String tag) {
        long time = System.currentTimeMillis();
        ApiService.apiService.getLoggedAccountInfo(AppUtils.generateHeaders())
                .enqueue(new Callback<JsonResponseBase<Map<String, Object>>>() {
                    @Override
                    public void onResponse(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Response<JsonResponseBase<Map<String, Object>>> response) {
                        if (response.code() != 200 || response.body() == null) {
                            Log.e(tag, "Get logged account info " + time + " failed:" + response.message());
                        } else {
                            Log.i(tag, "Get logged account info " + time + " success");
                            Map<String, Object> data = response.body().getData();
                            Map<?, ?> loggedAccount = (Map<?, ?>) data.get("loggedAccount");
                            double accountId = Double.parseDouble(String.valueOf(loggedAccount.get("accountId")));

                            SharedPreferenceUtil.setInteger(AppConstants.ACCOUNT_ID, (int) accountId);
                            SharedPreferenceUtil.setString(AppConstants.AVATAR, (String) loggedAccount.get("avatar"));
                            SharedPreferenceUtil.setString(AppConstants.NAME, (String) loggedAccount.get("name"));
                            SharedPreferenceUtil.setString(AppConstants.GENDER, (String) loggedAccount.get("gender"));
                            SharedPreferenceUtil.setString(AppConstants.DOB, (String) loggedAccount.get("dob"));
                            SharedPreferenceUtil.setString(AppConstants.EMAIL, (String) loggedAccount.get("email"));
                            SharedPreferenceUtil.setString(AppConstants.PHONE, (String) loggedAccount.get("phone"));
                            SharedPreferenceUtil.setString(AppConstants.CREATED_DATE, (String) loggedAccount.get("createdDate"));
                        }
                    }

                    @Override
                    public void onFailure(
                            @NonNull Call<JsonResponseBase<Map<String, Object>>> call,
                            @NonNull Throwable t) {
                        Log.e(tag, "Get logged account info " + time + " error");
                        Log.getStackTraceString(t);
                    }
                });
    }
}
