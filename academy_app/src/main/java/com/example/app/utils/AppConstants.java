package com.example.app.utils;

public class AppConstants {
    public static final String TOKEN = "token";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String REMEMBER = "remember";
    public static final String DEFAULT_PASSWORD = "Abc@1234";

    public static final String ACCOUNT_ID = "accountId";
    public static final String AVATAR = "avatar";
    public static final String NAME = "name";
    public static final String GENDER = "gender";
    public static final String DOB = "dob";
    public static final String PHONE = "phone";
    public static final String EMAIL = "email";
    public static final String ROLE = "role";
    public static final String CREATED_DATE = "createdDate";
}
