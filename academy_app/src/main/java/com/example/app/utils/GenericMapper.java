package com.example.app.utils;

import android.os.Build;

import androidx.annotation.RequiresApi;

import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;

public class GenericMapper {

    private final ModelMapper modelMapper;

    public GenericMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public <S, T> List<T> mapToListOfType(List<S> source, Class<T> targetClass) {
        if (source == null || source.isEmpty()) {
            return null;
        }
        return source.stream()
                .map(item -> modelMapper.map(item, targetClass))
                .collect(Collectors.toList());
    }
}
